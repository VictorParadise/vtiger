package com.gama.vtiger.Module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    private SharedPreferences prefsUser,prefsInternet;

    public Session(Context cntx) {
        prefsUser = PreferenceManager.getDefaultSharedPreferences(cntx);
        prefsInternet = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    //---***************************************
    //------ session type login user.
    //---***************************************
    public void setUser(String userId,String sessionName) {
        prefsUser.edit().putString("sessionName", sessionName).commit();
        prefsUser.edit().putString("userId", userId).commit();
    }

    public String[] getUser() {
        String[] infoDR = {prefsUser.getString("userId",""),prefsUser.getString("sessionName","")};
        return infoDR;
    }

    public  void clearUser()
    {
        prefsUser.edit().clear();
    }


    //---***************************************
    //------ session Check Internet.
    //---***************************************
    public void setInternetState(boolean state) {
        prefsInternet.edit().putBoolean("state", state).commit();
    }

    public boolean getInternetState() {
        boolean stateInternet = prefsInternet.getBoolean("state",false);
        return stateInternet;
    }

    public  void clearInternetState()
    {
        prefsInternet.edit().clear();
    }

}