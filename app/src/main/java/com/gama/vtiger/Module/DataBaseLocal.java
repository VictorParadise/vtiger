package com.gama.vtiger.Module;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by victor on 11/02/2019.
 */
public class DataBaseLocal extends SQLiteOpenHelper {
    public static final String DB_NAME = "VTiger.db";
    public static final String TBL_infoUser = "tbl_infoUser";

    public DataBaseLocal(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" CREATE TABLE " + TBL_infoUser +
                        "(id INTEGER PRIMARY KEY AUTOINCREMENT ," +
                        "label TEXT,"+
                        "data TEXT)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL( "DROP TABLE IF EXISTS " + TBL_infoUser );

        onCreate(db);
    }

    public boolean insert_InfoUser( String label , String data)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put("label",label );
        cv.put("data",data );


        long result = db.insert( TBL_infoUser , null , cv );

        if( result == -1 )
            return false;
        else
            return true;
    }

//    public boolean deleteDoctor( String id )
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        long result = db.delete(TBL_Doctor, "Id = ?", new String[]{id});
//
//        if( result == 0 )
//            return false;
//        else
//            return true;
//    }



    public Cursor showAll_InfoUser()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor result = db.rawQuery("select * from " + TBL_infoUser, null);

        return  result;
    }

//    public Cursor getPassPhonNumber()
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor result = db.rawQuery( "select password,mobile from " + TBL_NAME , null );
//
//        return  result;
//    }

//    public boolean updateFlagRemember(String flag)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues cv = new ContentValues();
//        cv.put("flagRememberLogin", flag);
//
//
//        long result = db.update(TBL_Doctor, cv, "id = ?", new String[]{"1"});
//
//        if( result < 1 )
//            return false;
//        else
//            return true;
//    }


//    public Cursor showDiseases(String Diseasesid)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor result = db.rawQuery( "select * from " + TBL_Doctor +" where Diseasesid=?", new String[] {Diseasesid} );
//
//        return  result;
//    }


//    public void deleteTable()
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
////        db.execSQL("DROP TABLE IF EXISTS " + TBL_Secretary);
////        db.execSQL("DROP TABLE IF EXISTS " + TBL_Doctor);
//        db.execSQL( "DROP TABLE IF EXISTS " + TBL_Doctor );
//        db.execSQL( "DROP TABLE IF EXISTS " + TBL_Secretary );
//        onCreate(db);
//    }




    //***********************************************************************************************
    //***********************************************************************************************
    //************************************** TBL_Advice *********************************************
    //***********************************************************************************************
    //***********************************************************************************************

//    public boolean insert_TBL_Advice( String adviceid,String advice_groupid,String message,String status,String send_by,String date,String name)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues cv = new ContentValues();
//        cv.put("adviceid", adviceid);
//        cv.put("advice_groupid", advice_groupid);
//        cv.put("message", message);
//        cv.put("status", status);
//        cv.put("send_by", send_by);
//        cv.put("date", date);
//        cv.put("name", name);
//
//
//        long result = db.insert(TBL_Advice, null, cv);
//
//        if( result == -1 )
//            return false;
//        else
//            return true;
//    }
//
//
//    public boolean update_TBL_Advice( String adviceid,String status)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues cv = new ContentValues();
//        cv.put("adviceid", adviceid);
//        cv.put("status", status);
//
//        long result = db.update(TBL_Advice, cv, "adviceid = ?", new String[]{adviceid});
//
//        if( result < 1 )
//            return false;
//        else
//            return true;
//    }
//
//    public Cursor showAllData_TBL_AdviceByid(String adviceid)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor result = db.rawQuery("select * from " + TBL_Advice +" where adviceid=?", new String[] {adviceid} );
//
//        return  result;
//    }
//
//    public Cursor showAllData_TBL_AdviceByGroupid(String advice_groupid)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor result = db.rawQuery("select * from " + TBL_Advice +" where advice_groupid=?", new String[] {advice_groupid} );
//
//        return  result;
//    }
//
//    public Cursor showAllData_TBL_Advice()
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor result = db.rawQuery("select * from " + TBL_Advice, null);
//
//        return  result;
//    }

}
