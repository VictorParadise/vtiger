package com.gama.vtiger.DataList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class importMainMenu {

    JSONObject subMenu = new JSONObject();

    public JSONObject getMarketing() throws JSONException {
        subMenu.put("Campaigns", "{ 'isEntity': true ,'label':'کمپین های تبلیغاتی','singular':'کمپین تبلیغاتی'}");
        subMenu.put("Leads", "{ 'isEntity': true ,'label':'سرنخ های فروش','singular':'سرنخ فروش'}");
        subMenu.put("Contacts", "{ 'isEntity': true ,'label':'مخاطبین','singular':'مخاطب'}");
        subMenu.put("Accounts", "{ 'isEntity': true ,'label':'سازمان ها','singular':'سازمان'}");

        return subMenu;
    }

    public JSONObject getSale() throws JSONException {
        subMenu.put("Potentials", "{ 'isEntity': true ,'label':'فرصت های فروش','singular':'فرصت فروش'}");
        subMenu.put("Quotes", "{ 'isEntity': true ,'label':'پیش فاکتورها','singular':'پیش فاکتور'}");
        subMenu.put("Products", "{ 'isEntity': true ,'label':'محصولات','singular':'محصول'}");
        subMenu.put("Services", "{ 'isEntity': true ,'label':'سرویس ها','singular':'سرویس'}");
        subMenu.put("SMSNotifier", "{ 'isEntity': true ,'label':'سامانه ارسال پیام کوتاه','singular':'اطلاعات پیامک'}");
        subMenu.put("Contacts", "{ 'isEntity': true ,'label':'مخاطبین','singular':'مخاطب'}");
        subMenu.put("Accounts", "{ 'isEntity': true ,'label':'سازمان ها','singular':'سازمان'}");

        return subMenu;
    }

    public JSONObject getStore() throws JSONException {
        subMenu.put("Products", "{ 'isEntity': true ,'label':'محصولات','singular':'محصول'}");
        subMenu.put("Services", "{ 'isEntity': true ,'label':'سرویس ها','singular':'سرویس'}");
        subMenu.put("PriceBooks", "{ 'isEntity': true ,'label':'دفترچه های قیمت','singular':'دفترچه قیمت'}");
        subMenu.put("Invoice", "{ 'isEntity': true ,'label':'فاکتورها','singular':'فاکتور'}");
        subMenu.put("SalesOrder", "{ 'isEntity': true ,'label':'سفارش های فروش','singular':'سفارش فروش'}");
        subMenu.put("PurchaseOrder", "{ 'isEntity': true ,'label':'سفارشات خرید','singular':'سفارش خرید'}");
        subMenu.put("Vendors", "{ 'isEntity': true ,'label':'تامین کنندگان','singular':'تامین کننده'}");
        subMenu.put("Contacts", "{ 'isEntity': true ,'label':'مخاطبین','singular':'مخاطب'}");
        subMenu.put("Accounts", "{ 'isEntity': true ,'label':'سازمان ها','singular':'سازمان'}");

        return subMenu;
    }

    public JSONObject getSupport() throws JSONException {
        subMenu.put("HelpDesk", "{ 'isEntity': true ,'label':'تیکت ها','singular':'تیکت'}");
        subMenu.put("Faq", "{ 'isEntity': true ,'label':'سوالات متداول','singular':'سوال متداول'}");
        subMenu.put("ServiceContracts", "{ 'isEntity': true ,'label':'قراردادهای خدمات','singular':'قرارداد خدمات'}");
        subMenu.put("Assets", "{ 'isEntity': true ,'label':'پرونده کالاها','singular':'پرونده کالا'}");
        subMenu.put("SMSNotifier", "{ 'isEntity': true ,'label':'سامانه ارسال پیام کوتاه','singular':'اطلاعات پیامک'}");
        subMenu.put("Contacts", "{ 'isEntity': true ,'label':'مخاطبین','singular':'مخاطب'}");
        subMenu.put("Accounts", "{ 'isEntity': true ,'label':'سازمان ها','singular':'سازمان'}");

        return subMenu;
    }

    public JSONObject getProjects() throws JSONException {
        subMenu.put("Project", "{ 'isEntity': true ,'label':'پروژه ها','singular':'پروژه'}");
        subMenu.put("ProjectTask", "{ 'isEntity': true ,'label':'وظیفه پروژه','singular':'وظیفه پروژه'}");
        subMenu.put("ProjectMilestone", "{ 'isEntity': true ,'label':'فاز پروژه','singular':'فاز پروژه'}");
        subMenu.put("Contacts", "{ 'isEntity': true ,'label':'مخاطبین','singular':'مخاطب'}");
        subMenu.put("Accounts", "{ 'isEntity': true ,'label':'سازمان ها','singular':'سازمان'}");

        return subMenu;
    }
}
