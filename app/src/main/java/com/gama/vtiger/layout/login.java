package com.gama.vtiger.layout;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;

import com.gama.vtiger.Module.CheckConnectivity;
import com.gama.vtiger.Module.DataBaseLocal;
import com.gama.vtiger.R;

import com.gama.vtiger.Module.Session;
import com.gama.vtiger.language.LocaleManager;
import com.gama.vtiger.webService.MyApi;
import com.gama.vtiger.webService.RetrofitClient;

import android.util.Patterns;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends AppCompatActivity {

    // protected String username ="admin", USERACCESSKEY="m2rT1SjxQe1W3XxG";
    protected EditText txtPass,txtUser,txtUrl;
    protected TextView txtLogin;
    protected RetrofitClient retrofit_client ;
    protected MyApi apiService = null;
    protected ProgressDialog progressDialog;
    protected Session session;
    protected boolean stateInternet;
    protected String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        registerReceiver(
                new CheckConnectivity(),
                new IntentFilter(
                        ConnectivityManager.CONNECTIVITY_ACTION));
        session = new Session(login.this);

        txtPass = (EditText) findViewById(R.id.txtPass);
        txtUser = (EditText) findViewById(R.id.txtUser);
        txtUrl = (EditText) findViewById(R.id.txtUrl);
        txtLogin = (TextView) findViewById(R.id.txt_login);

        final AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);

        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(buttonClick);
//                setNewLocale(login.this, "fa");
                stateInternet = session.getInternetState();
                if (stateInternet) {
                    callCheckValidate();
                }else
                {
                    Toast.makeText(login.this,R.string.turnOnInternet,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
    }

    //---- change language
    private void setNewLocale(AppCompatActivity mContext, @LocaleManager.LocaleDef String language) {
        LocaleManager.setNewLocale(this, language);
        Intent intent = mContext.getIntent();
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    //---- check user & pass and validation "url" ::: is 'true' condition connect -> to api.
    public void callCheckValidate() {
        if (txtUrl.length() > 0) {
            if (URLUtil.isValidUrl(txtUrl.getText().toString()) && Patterns.WEB_URL.matcher(txtUrl.getText().toString()).matches()) {
                if (txtUser.length() > 0 && txtPass.length() > 0) {
                    //----- progressDialog white icon.
                    progressDialog = new ProgressDialog(login.this);
                    progressDialog.setMessage(getString(R.string.witingConnect));
                    progressDialog.show();

                    //----- create api for connect.
                    url =txtUrl.getText().toString();

                    //-------- check last char is -> '/'.
                    String url_lastChar = url.substring(url.length() - 1);
                    if (!url_lastChar.equalsIgnoreCase("/"))
                    {
                        url = txtUrl.getText().toString()+"/";
                    }

                    retrofit_client = new RetrofitClient(url);
                    Call<ResponseBody> call = null;
                    try {
                        apiService = retrofit_client
                                .getInstance()
                                .getMyApi();
                        call = apiService.getdata("getchallenge", txtUser.getText().toString());
                        try {
                                call.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        try {
                                            //------ message ="url not found".
                                            if (response.code() == 404) {
                                                progressDialog.dismiss();
                                                Toast.makeText(login.this, R.string.urlNotFind, Toast.LENGTH_LONG).show();

                                                //-----destruct and clear class java.
                                                System.gc();
                                                System.runFinalization();
                                            } else {
                                                callFunctionGetData(response);
                                            }
                                        } catch (Exception e) {
                                            progressDialog.dismiss();
                                            e.printStackTrace();
                                            System.gc();
                                            System.runFinalization();
                                            return;
                                        }
                                    }
                                    //------------ message = "url not found"
                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        progressDialog.dismiss();
                                        Toast.makeText(login.this, R.string.urlNotFind, Toast.LENGTH_LONG).show();
                                    }
                                });
                        }catch (Exception e)
                        {
                            //--------- url not found .
                            progressDialog.dismiss();
                            System.gc();
                            System.runFinalization();
                            Toast.makeText(login.this, R.string.urlNotFind, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        //------------- message ="connection failed!"
                        progressDialog.dismiss();
                        Toast.makeText(login.this, R.string.connectionFild, Toast.LENGTH_SHORT).show();
                        System.gc();
                        System.runFinalization();
                    }

                } else {
                    //--------- message ="please enter user and pass."
                    progressDialog.dismiss();
                    Toast.makeText(login.this, R.string.enterUserPass, Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                //-----------message ="connection failed"
                Toast.makeText(login.this, R.string.connectionFild, Toast.LENGTH_SHORT).show();
                System.gc();
                System.runFinalization();
            }
        }
        else {
            //---------- message ="please enter url crm."
            Toast.makeText(login.this, R.string.enterUrl, Toast.LENGTH_SHORT).show();
        }
    }

    //----- check is true 'response' -> to md5
    private void callFunctionGetData(Response<ResponseBody> response){
        String s = null;
        try {
            s = response.body().string();
        } catch (IOException e) {
            progressDialog.dismiss();
            e.printStackTrace();
        }
        try {
            JSONObject jsonMain = new JSONObject(s);
            //true response.
            //---- get field => result & token.
            if (jsonMain.getBoolean("success")) {
                JSONObject data = jsonMain.getJSONObject("result");
                String token = data.getString("token").toString();
                //-------- function cast -> md5.
                String AccessKey = md5(token + txtPass.getText().toString());
                //------ get token => call func-> 'callGetSession' for get session.
                callGetSession(AccessKey);

            } else //error in response.
            {
                //---- message = "username does not exists."
                progressDialog.dismiss();
                Toast.makeText(login.this, R.string.ExistsUser, Toast.LENGTH_LONG).show();
                System.gc();
                System.runFinalization();
            }
        }//end fetch json object.
        catch (JSONException e) {
            //-------- message = "url not found"
            progressDialog.dismiss();
            e.printStackTrace();
            Toast.makeText(login.this, R.string.urlNotFound, Toast.LENGTH_LONG).show();
        }
    }


    //----- convert string to md5.
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public void callGetSession(String AccessKey)
    {
//        MyApi apiService = retrofit_client
//                .getInstance()
//                .getMyApi();

        //------ Send data to webService.
        Call<ResponseBody> call = apiService.getSession("login", txtUser.getText().toString(),AccessKey);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                String s = null;
                try {
                    s = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject jsonMain = new JSONObject(s);
                    //true response -> get fields => userid & sessionName.
                    if (jsonMain.getBoolean("success")) {
                        try {
                            String sessionName, userId;
                            JSONObject data = jsonMain.getJSONObject("result");
                            sessionName = data.getString("sessionName").toString();
                            userId = data.getString("userId").toString();
                            //---- create session and set data to session.
                            session.setUser(userId,sessionName);

                            //----- getInfoUSer (get field 'language' to 'change language app').
                            getInfoUser(sessionName,userId);

                        }catch (Exception e)
                        {
                            //------message = "error in received data"
                            Toast.makeText(login.this,R.string.errorGetData,Toast.LENGTH_SHORT).show();
                        }
                    } else //error in response.
                    {
                        //------- message = "invalid username or password"
                        Toast.makeText(login.this, R.string.invalidUserPass, Toast.LENGTH_LONG).show();
                        System.gc();
                        System.runFinalization();
                    }
                }//end fetch json object.
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }//end get response webservice.

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //-------- message = "connection failed"
                Toast.makeText(login.this, R.string.errorGetSession, Toast.LENGTH_LONG).show();
                // Log error here since request failed
            }
        });//end call response webservice.
    }


    public void getInfoUser(String sessionName,String userId)
    {
//        MyApi apiService = retrofit_client
//                .getInstance()
//                .getMyApi();

        //------ Send data to webService.
        Call<ResponseBody> call = apiService.getQuery("query", "SELECT * FROM Users where id="+userId+";",sessionName);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                String s = null;
                try {
                    s = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    JSONObject jsonMain = new JSONObject(s);
                    //true response -> get fields => userAllInfo -> sample -> language.
                    if (jsonMain.getBoolean("success")) {
                        try {
                            String language;

                            //------------------ get field 'result' --> (jsonArray)
                            JSONArray JsonArray_result = jsonMain.getJSONArray("result");
                            //---------------get index[0] ---> data content ->
                            JSONObject ObjectInfoUser = JsonArray_result.getJSONObject(0);
                            //--------------- get field -> language.
                            language = ObjectInfoUser.getString("language");
                            //----------- change language application.
                            setNewLocale(login.this, language);
                            //----------- insert data to table local.
                            DataBaseLocal DB_Local =new DataBaseLocal(login.this);
                            boolean state = DB_Local.insert_InfoUser("infoUser", ObjectInfoUser.toString() );

                            if (!state)
                            {
                                Toast.makeText(login.this,R.string.errorGetData,Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                //----- next -> open page MainIndex.
                                Intent i = new Intent(login.this, main.class);
                                startActivity(i);
                            }

                        }catch (Exception e)
                        {
                            //------ message = "error in received data"
                            Toast.makeText(login.this,R.string.errorGetData,Toast.LENGTH_SHORT).show();
                        }
                    } else //error in response.
                    {
                        //------- message = "error in received data"
                        Toast.makeText(login.this, R.string.errorGetData, Toast.LENGTH_LONG).show();
                        System.gc();
                        System.runFinalization();
                    }
                }//end fetch json object.
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }//end get response webservice.

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //-------- message = "connection failed"
                Toast.makeText(login.this, R.string.errorGetSession, Toast.LENGTH_LONG).show();
                // Log error here since request failed
            }
        });//end call response webservice.
    }

}
