package com.gama.vtiger.layout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.gama.vtiger.Adapter.RecycleAdapter_mainMenu;
import com.gama.vtiger.DataList.DataListMainMenu;
import com.gama.vtiger.R;
import com.gama.vtiger.language.LocaleManager;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class main extends AppCompatActivity {

    protected List<DataListMainMenu> DA_MainMenu=new ArrayList<>();
    protected RecyclerView recycler_mainMenu;

    public static Activity tr7;
    public RecyclerView recycler_SubMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tr7 = this;
        //------- set text title appBar.
        TextView titlePage =(TextView)findViewById(R.id.textTitlePage);
        titlePage.setText(R.string.app_name);

        recycler_mainMenu =(RecyclerView)findViewById(R.id.recycler_mainMenu);
        recycler_SubMenu =(RecyclerView)findViewById(R.id.recycler_SubMenu);

        //--////////////////////////////////////////////////////////////////////////
        //--///////////////--------   array Main Menu -----------//////////////////.
        //--////////////////////////////////////////////////////////////////////////
        JSONObject StaticMainMenu = new JSONObject();
        try {
            StaticMainMenu.put("0","{ 'Title': '"+getString(R.string.marketing)+"' ,'url':'marketing','value':'Marketing'}");
            StaticMainMenu.put("1","{ 'Title': '"+getString(R.string.sale)+"' ,'url':'sale','value':'Sale'}");
            StaticMainMenu.put("2","{ 'Title': '"+getString(R.string.store)+"' ,'url':'store','value':'Store'}");
            StaticMainMenu.put("3","{ 'Title': '"+getString(R.string.support)+"' ,'url':'support','value':'Support'}");
            StaticMainMenu.put("4","{ 'Title': '"+getString(R.string.projects)+"' ,'url':'projects','value':'Projects'}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < StaticMainMenu.length(); i++) {
            DataListMainMenu meData = new DataListMainMenu();
            try {
                String StringArray = StaticMainMenu.getString(String.valueOf(i));
                JSONObject J_Object = new JSONObject(StringArray);

                meData.txtTitleMAinMenu = J_Object.getString("Title");
                meData.url = J_Object.getString("url");
                meData.value = J_Object.getString("value");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            DA_MainMenu.add(meData);
        }

        // set up the RecyclerView
        recycler_mainMenu.setLayoutManager(new LinearLayoutManager(main.this));
        RecycleAdapter_mainMenu adapter = new RecycleAdapter_mainMenu(main.this, DA_MainMenu);
        //adapter.setClickListener(this);
        recycler_mainMenu.setAdapter(adapter);
        recycler_mainMenu.getAdapter().notifyDataSetChanged();


    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
    }
}
