package com.gama.vtiger.webService;

import android.content.Context;
import android.media.ExifInterface;
import android.util.Log;
import android.widget.Toast;

import java.io.Closeable;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hamed on 2/13/18.in RetrofitSampleApp
 */

public class RetrofitClient{



    private static RetrofitClient mInstance;
    private static String URLBase;
    private Retrofit retrofit;

    public RetrofitClient(String url)
    {
        try {
            this.URLBase = url;
            retrofit = new Retrofit.Builder()
                    .baseUrl(URLBase)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        catch (Exception e ){
            return;
        }
    }

    public static synchronized RetrofitClient getInstance()
    {
        try {
            mInstance = new RetrofitClient(URLBase);
            return mInstance;
        }
        catch (Exception e){return null;}

    }

    public MyApi getMyApi()
    {
        try {
            return retrofit.create(MyApi.class);
        }catch (Exception e){
            return null;
        }
    }

    public void finalize() {
        URLBase = null;
    }
}