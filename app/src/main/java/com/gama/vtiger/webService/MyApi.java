package com.gama.vtiger.webService;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by hamed on 2/3/18.
 */

public interface MyApi {


    @GET("webservice.php")
    Call<ResponseBody> getdata(
            @Query("operation") String operation,
            @Query("username") String username
    );


    @FormUrlEncoded
    @POST("webservice.php")
    Call<ResponseBody> getSession(
            @Field("operation") String operation,
            @Field("username") String username,
            @Field("accessKey") String accessKey
    );


    @GET("webservice.php")
    Call<ResponseBody> getQuery(
            @Query("operation") String operation,
            @Query("query") String query,
            @Query("sessionName") String sessionName
    );


//-------------------------------------------
    @FormUrlEncoded
    @POST("get_visit_list")
    Call<ResponseBody> get_visit_list(
            @Header("Accept") String Accept,
            @Header("Authorization") String Authentication,
            @Field("id") String id,
            @Field("type") String type,
            @Field("date") String date
    );


    @FormUrlEncoded
    @POST("set_patient_presence")
    Call<ResponseBody> set_patient_presence(
            @Header("Accept") String Accept,
            @Header("Authorization") String Authentication,
            @Field("clinicid") String clinicid,
            @Field("presence") Integer presence,
            @Field("clinic_visit_managerid") String clinic_visit_managerid
    );



    @Multipart
    @POST("set_patient_prescription")
    Call<ResponseBody> set_patient_prescription(
            @HeaderMap Map<String, String> token,
            @Part("clinicid") String clinicid,
            @Part("patient_caseid") String patient_caseid,
            @Part("clinic_visit_manager") String clinic_visit_manager,
            @Part("comment") String comment,
            @Part MultipartBody.Part file,
            @Part MultipartBody.Part file1
    );


    @Multipart
    @POST("set_learning_video")
    Call<ResponseBody> set_learning_video(
            @HeaderMap Map<String, String> token,
            @Part("doctorid") String doctorid,
            @Part("title") String title,
            @Part("title_en") String title_en,
            @Part("title_ar") String title_ar,
            @Part("video_url") String video_url,
            @Part("tag") String tag,
            @Part("comment") String comment,
            @Part("comment_en") String comment_en,
            @Part("comment_ar") String comment_ar,
            @Part MultipartBody.Part file
    );

}