package com.gama.vtiger.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.gama.vtiger.layout.main;
import com.gama.vtiger.DataList.DataListMainMenu;
import com.gama.vtiger.DataList.DataLisSubMenu;
import com.gama.vtiger.R;
import com.gama.vtiger.DataList.importMainMenu;
import com.squareup.picasso.Picasso;


import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by victor on 07/01/2019.
 */
public class RecycleAdapter_mainMenu extends RecyclerView.Adapter<RecycleAdapter_mainMenu.ViewHolder> {


    private LayoutInflater mInflater;
    //private ItemClickListener mClickListener;
    List<DataListMainMenu> mData= Collections.emptyList();
    private Context context;
    String mItem;
    private RecyclerView recycler_SubMenu = main.tr7.findViewById(R.id.recycler_SubMenu);
    private List<DataLisSubMenu> DA_SubMenu =new ArrayList<>();
    // data is passed into the constructor
    public RecycleAdapter_mainMenu(Context context, List<DataListMainMenu> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.navigation_right_main, parent, false);
        return new ViewHolder(view);
    }


    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final DataListMainMenu current=mData.get(position);
        holder.txtTitleMAinMenu.setText(current.txtTitleMAinMenu);

        //---------- read image by dynamically.-----------------------
        Class res = R.drawable.class;
        Field field = null;
        try {
            field = res.getField(current.url);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        try {
            int drawableId = field.getInt(null);
            Picasso.with(context).load(drawableId).resize(30, 30).into(holder.iconImage);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        //---------------------- end section image reading.----------

        holder.LMAinMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearData();
                JSONObject JOb_All_SubMenu ;
                //------------ create subMenu.
                try {

                    JOb_All_SubMenu = getSubMenuList(current.value);
                    Iterator<String> keys= JOb_All_SubMenu.keys();
                    while (keys.hasNext())
                    {
                        String keyValue = (String)keys.next();
                        JSONObject subMenu = new JSONObject(JOb_All_SubMenu.getString(keyValue));

                        DataLisSubMenu meDataSubMEnu = new DataLisSubMenu();

                        meDataSubMEnu.isEntity = subMenu.getString("isEntity");
                        meDataSubMEnu.label = subMenu.getString("label");
                        meDataSubMEnu.singular = subMenu.getString("singular");
                        meDataSubMEnu.value = keyValue;//-------title english sub menu.

                        DA_SubMenu.add(meDataSubMEnu);
                    }
                    // set up the RecyclerView
                    recycler_SubMenu.setLayoutManager( new LinearLayoutManager(main.tr7));
                    RecycleAdapter_subMenu adapter = new RecycleAdapter_subMenu(main.tr7, DA_SubMenu);
                    //adapter.setClickListener(this);
                    recycler_SubMenu.setAdapter(adapter);
                    recycler_SubMenu.getAdapter().notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void clearData() {
        DA_SubMenu.clear();
        recycler_SubMenu.setAdapter(null);
        recycler_SubMenu.removeAllViewsInLayout(); // clear list
    }

    public JSONObject getSubMenuList(String valueMenu) throws JSONException {
        //------Temp array fake.
        importMainMenu ImportMainMenu = new importMainMenu();
        JSONObject JO_SubMenu;
        boolean flagImport = true;
        switch (valueMenu){
            case "Marketing":
                JO_SubMenu = ImportMainMenu.getMarketing();
                break;
            case "Projects":
                JO_SubMenu = ImportMainMenu.getProjects();
                break;
            case "Sale":
                JO_SubMenu = ImportMainMenu.getSale();
                break;
            case "Store":
                JO_SubMenu = ImportMainMenu.getStore();
                break;
            case "Support":
                JO_SubMenu = ImportMainMenu.getSupport();
                break;
            default:
                JO_SubMenu = new JSONObject("error in get data");
                flagImport = false ;
                break;
        }

        return JO_SubMenu;
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitleMAinMenu;
        ImageView iconImage;
        LinearLayout LMAinMenu;
//        RecyclerView recycler_SubMenu;
        ViewHolder(View itemView) {
            super(itemView);
            txtTitleMAinMenu = (TextView) itemView.findViewById(R.id.txtTitleMAinMenu);
            iconImage = (ImageView) itemView.findViewById(R.id.iconImage);
            LMAinMenu = (LinearLayout) itemView.findViewById(R.id.LMAinMenu);
//            recycler_SubMenu = (RecyclerView) itemView.findViewById(R.id.recycler_SubMenu);
            // itemView.setOnClickListener(this);
        }

        public void setItem(String item) {
            mItem = item;
            // mTextView.setText(item);
        }


//        @Override
//        public void onClick(View view) {
//
////            final Intent i = new Intent(view.getContext(),varzesh1.class);
////            startActivity(i);
//            if (mClickListener != null)
//            {
//                mClickListener.onItemClick(view, getAdapterPosition());
//
//            };
//
//        }
    }


//    // allows clicks events to be caught
//    void setClickListener(ItemClickListener itemClickListener) {
//        this.mClickListener = itemClickListener;
//    }
//
//    // parent activity will implement this method to respond to click events
//    public interface ItemClickListener {
//        void onItemClick(View view, int position);
//    }
}