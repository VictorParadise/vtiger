package com.gama.vtiger.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gama.vtiger.DataList.DataLisSubMenu;
import com.gama.vtiger.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by victor on 07/01/2019.
 */
public class RecycleAdapter_subMenu extends RecyclerView.Adapter<RecycleAdapter_subMenu.ViewHolder> {

    private LayoutInflater mInflater;
    //private ItemClickListener mClickListener;
    List<DataLisSubMenu> mData= Collections.emptyList();
    private Context context;
    String mItem;

    // data is passed into the constructor
    public RecycleAdapter_subMenu(Context context, List<DataLisSubMenu> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.navigation_left_main, parent, false);
        return new ViewHolder(view);
    }


    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final DataLisSubMenu current=mData.get(position);
        holder.txtTitleSubMenu.setText(current.label);


        holder.LSubMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }



    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitleSubMenu;
        LinearLayout LSubMenu;
        ViewHolder(View itemView) {
            super(itemView);
            txtTitleSubMenu = (TextView) itemView.findViewById(R.id.txtTitleSubMenu);
            LSubMenu = (LinearLayout) itemView.findViewById(R.id.LSubMenu);
            // itemView.setOnClickListener(this);
        }

        public void setItem(String item) {
            mItem = item;
            // mTextView.setText(item);
        }


//        @Override
//        public void onClick(View view) {
//
////            final Intent i = new Intent(view.getContext(),varzesh1.class);
////            startActivity(i);
//            if (mClickListener != null)
//            {
//                mClickListener.onItemClick(view, getAdapterPosition());
//
//            };
//
//        }
    }


//    // allows clicks events to be caught
//    void setClickListener(ItemClickListener itemClickListener) {
//        this.mClickListener = itemClickListener;
//    }
//
//    // parent activity will implement this method to respond to click events
//    public interface ItemClickListener {
//        void onItemClick(View view, int position);
//    }
}